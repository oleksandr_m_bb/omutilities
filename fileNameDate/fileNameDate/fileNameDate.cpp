// fileNameDate.cpp : Defines the entry point for the console application.
//

#include <SDKDDKVer.h>
#undef _WIN32_WINNT
#define _WIN32_WINNT 0x0601

#include <cstdio>
#include <Windows.h>

namespace {
    static const char DELIM_DATE[] = "_";
    static const char DELIM_TIMEDATE[] = "_";
    static const char DELIM_TIME[] = "-";
    static const char DELIM_MILLI[] = ".";

    class TimeEntity : public SYSTEMTIME {
        static constexpr int _bufferSize = 32;
        char _buffer[_bufferSize] = {};
        //TODO: make union
        int _position = 0;
        int _size = 0;

        //std::stringstream textStream;

        void append(WORD w) {
            if (w < 10) {
                _position += snprintf(_buffer + _position, _bufferSize - _position, "0%u", w);
            }
            else {
                _position += snprintf(_buffer + _position, _bufferSize - _position, "%u", w);
            }
        }
        void append(const char *s) {
            _position += snprintf(_buffer + _position, _bufferSize - _position, "%s", s);
        }
        void append(const char c) {
            _position += snprintf(_buffer + _position, _bufferSize - _position, "%c", c);
        }

        template<typename T>
        void format(T value) {
            append(value);
        }

        template<typename T, typename... Targs>
        void format(T value, Targs... targs)
        {
            append(value);
            format(targs...);
        }

    public:
        TimeEntity() {
            GetLocalTime(static_cast<LPSYSTEMTIME>(this));
            format(wYear, DELIM_DATE, wMonth, DELIM_DATE, wDay, DELIM_TIMEDATE,
                wHour, DELIM_TIME, wMinute, DELIM_TIME, wSecond, DELIM_MILLI, wMilliseconds);

        }
        ~TimeEntity() {
        }
        const char* string() const {
            return _buffer;
        }
    };
}


int main(int, char)
{
    const TimeEntity entity;
    int rv = puts(entity.string());
    return rv > 1 ? 0 : 1;
}